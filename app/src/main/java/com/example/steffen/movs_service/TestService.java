package com.example.steffen.movs_service;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import java.text.SimpleDateFormat;

/**
 * Created by Steffen on 07.03.2017.
 */

public class TestService extends IntentService {
    /**
     * @param name Used to name the worker thread, important only for debugging.
     */
    public TestService(String name) {
        super(name);
    }

    public TestService() {
        super("TestService");
    }


    //Will be called when service is executed
    @Override
    protected void onHandleIntent(Intent workIntent) {
        String dataString = workIntent.getDataString();
        Log.wtf("MyCustomService", new SimpleDateFormat("HH:mm:ss").format(System.currentTimeMillis()) + ": Service did something with " + dataString + ".");


        /*
        * Creates a new Intent containing a Uri object
        * BROADCAST_ACTION is a custom Intent action
        */
        /*Intent localIntent =
                new Intent(Constants.BROADCAST_ACTION)
                        // Puts the status into the Intent
                        .putExtra(Constants.EXTENDED_DATA_STATUS, Uri.parse("Party"));
        // Broadcasts the Intent to receivers in this app.
        LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);*/

    }


}
