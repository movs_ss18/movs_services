package com.example.steffen.movs_service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.format.DateUtils;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by Steffen on 07.03.2017.
 */

public class ServiceReceiver extends BroadcastReceiver {

    // Prevents instantiation
    public ServiceReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        Intent testServiceIntent = new Intent(context, TestService.class);
        testServiceIntent.setData(Uri.parse("Hello Alice"));
        PendingIntent testServicePendingIntent = PendingIntent.getService(context, 0, testServiceIntent, 0);
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);



        if ((action.equals(Intent.ACTION_BOOT_COMPLETED) || intent.getAction().equals("StartSaveService"))) {
            Log.wtf("WhatTheDroidService", " ***\n Boot komplett bzw. StartService recieved. \n ***");
            Toast.makeText(context, "Recieved (Start)", Toast.LENGTH_LONG).show();

            /*Intent i = new Intent(context, TestService.class);
            i.setData(Uri.parse("Hello"));
            context.startService(i);*/

            //Wie gross soll der Intervall sein?
            long interval = DateUtils.MINUTE_IN_MILLIS * 1; // Jede Minute

            //Wann soll der Service das erste Mal gestartet werden?
            long firstStart = System.currentTimeMillis();

            am.setRepeating(AlarmManager.RTC, firstStart, interval, testServicePendingIntent);
            //am.setRepeating(AlarmManager.RTC, firstStart, AlarmManager.INTERVAL_HOUR, testServicePendingIntent);//AlarmManager.INTERVAL_HOUR

            Log.wtf("WhatTheDroidService", "AlarmManager gesetzt");



        } else if (intent.getAction().equals("StopSaveService")) {
            Toast.makeText(context, "Recieved (Stop)", Toast.LENGTH_SHORT).show();

            am.cancel(testServicePendingIntent);


            Log.wtf("WhatTheDroidService", "AlarmManager entfernt");
        }
    }
}
